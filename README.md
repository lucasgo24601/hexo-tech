# Blog簡介
使用 [Hexo](https://hexo.io/zh-tw/) 部落格框架，其中主題為 [Butterfly](https://github.com/jerryc127/hexo-theme-butterfly)，部落格內主要紀錄個人學習紀錄，因為個人學習方式是**費曼學習法**，將學習的東西輸出後才會比較容易融會貫通，所以使用部落格方式記錄

# Blog
採用 Nodejs的 Hexo 框架，source 資料夾為 Markdown的個人筆記，使用 npm run build 即可轉換Markdown為index

# GCP (已廢棄)
1. 使用 Nginx 自動化部屬成 Docker Image
2. 使用 Google Cloud Compute Engine 自動化部屬 VM with docker image
3. 使用 Python 自動化部屬 靜態資源 to Google Cloud Storage
4. 使用 Let's encrypt With CloudFlare DNS 更新 SSL憑證
5. 使用 CloudFlare CDN API 清除並部屬新的靜態資源

![](https://i.imgur.com/ktDi0RD.png)
