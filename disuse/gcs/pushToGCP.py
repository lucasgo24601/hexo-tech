from google.cloud import storage
import os
import sys

API_JSON_NAME =  sys.argv[1] # "./apiKey.json"
BUCKET_NAME = sys.argv[2] # "lucasgo24601.com"
PUSH_FOLDRE_NAME = sys.argv[3] # "public"

# os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = API_JSON_NAME
storage_client = storage.Client()
bucket = storage_client.get_bucket(BUCKET_NAME)

# get all file name and path
filesName = []
def loop_folder(path):
    for (dirpath, dirnames, filenames) in os.walk(path):
        if len(filenames) ==0:
            continue
        for dirname in dirnames:
            loop_folder(dirpath + dirname)
        dirpath += '/'
        realPath = dirpath[len(PUSH_FOLDRE_NAME)+1:]
        names =[]
        for fileName in filenames:
            names.append(realPath +fileName)
        filesName.extend(names)

# remove all 
print('clean bucket {name}'.format(name=BUCKET_NAME))
bucket.delete_blobs( list(bucket.list_blobs()))

# push file
print('search files , folder = {path}'.format(path=PUSH_FOLDRE_NAME))
loop_folder(PUSH_FOLDRE_NAME)
total =len(filesName)
for index,file in enumerate(filesName):
    print("Path = {file} , {index}/{total}".format(file=file,index=index+1,total=total))
    blob = bucket.blob(file)
    blob.upload_from_filename("public/"+file)