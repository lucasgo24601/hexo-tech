---
title: 前端P.02：ECMA 腳本語言 JavaScript
date: 2020-02-14 17:54:00
categories: 前端系列
comments: true
cover: https://i.imgur.com/9qPDEZj.png
type: post
toc: true
toc_number: 250
copyright: false
tags: 
    - 前端歷史
    - ECMA
    - ECMAScript
    - JavaScript
    - 第一次瀏覽器大戰
    - IE
    - Netscape
    - 腳本語言
---
# 🥜 前情提要：
我們知道了瀏覽器的三大語言，HTML、JavaScript、CSS，今天來介紹其中的主角，JavaScript

# JavaScript：
![](https://i.imgur.com/sC6usCg.png)
> JavaScript是一門直譯型的弱型別程式語言，由ECMAScript規範的實現。

讓我們先暫停在這邊
![](https://i.imgur.com/KoC0wnI.jpg)

## 什麼是 直譯型語言
所謂的直譯型請參考「程式語言的誕生」，簡單說CPU執行到那一行才會翻譯成機器語言。

## 什麼是 弱型別
> 弱型別是一種程式設計語言的型別系統。

請參考以下程式碼
```
let num = 20
num = "我是Lucas"
```
這是一段JavaScript的範例，上面這段屬於合法程式，

1. 一開始是數字後來又被改成中文，因為弱型別允許變數可以隨時隨地的改變型別
2. 變數不需要宣告類型即可使用。

優點:
- 彈性靈活

缺點：
- 當變數發生型別改變時，效能吃重
- 容易會有BUG

## 什麼是 ECMAScript？
ECMAScript是由ECMA組織制定的腳本語言規範，代號為TC39，你可以把它當作是一個腳本語言的規格書，這個規範紀載了

1. 腳本語言應該要有個關鍵字，只要被他賦予的變數，就無法修改了
2. 腳本語言應該要有個能夠異步操作的關鍵字
3. 腳本語言應該要有個更簡易的製作函數的寫法

在 ECMAScript 制定完成這些後，他們做了什麼？答案是：「ECMA 他們什麼都不幹」。於是就有了JavaScript 這語言把ECMAScript實做出來：

1. const temp = 20; 
2. new Promise( Fun:Function)
3. var Fun = ()=> {}; 

JavaScript根據規範制定了上述的3點。

> PS: 此外，實作ECMAScript的人還有很多，例如ActionScript和JScript。

而ECMA他們甚至想要定義時間的作用，應該也考慮進火星的時間，因為JavaScript被廣泛使用，甚至連NASA的太空服軟件也是JavaScript寫的。
![](https://i.imgur.com/xyi9Z9I.png)

![](https://i.imgur.com/TWXAGxd.png)

由上大概可以知道 ECMAScript 創立了 JavaScript ，但其實不盡然

___ 
在我還沒出生時那時第一次瀏覽器大戰已經開打，微軟(IE) VS 網景(Netscape)
![](https://i.imgur.com/Ctzj7us.png)
在這場戰爭中還沒有標準化的概念，所以你用你的我用我的，造成很多網頁上寫明：

```
「用IE可達到最佳效果」
「用Netscape可達到最佳效果」
```

在大戰中網景公司認為網頁需要一種腳本語言，讓網頁設計師可以很容易地使用它來組裝圖片和外掛程式之類的元件，且程式碼可以直接編寫在網頁標記中。於是JavaScript因此而誕生。

而之所以取名"Java"Script只是因為當時Java很夯，沾點熱度而已，本質跟Java沒有任何關係。
![](https://i.imgur.com/jJvCXUX.png)

網景一推出大受好評，微軟也跟進自己設計了JScript，但也因此 你用你的、我用我的 這情況更嚴重，於是網景向ECMA提出標準，ECMA根據JavaScript制定了ECMAScript規範，當然ECMA會採用JScript和JavaScript的相容。

## 什麼是腳本語言？
> 腳本語言通常不需要經過編譯，並且可以直接在執行環境中運行。

而要符合這要求有很多語言：JavaScript、Python和Ruby都是常見的腳本語言。而這些語言就類似:

假設我們要 **走**、**跑**、**跳** 等等，這些動作在程式碼中都可以被定義為一個命令或是函數，然而這些操作必須由某個東西來驅動，像是如果沒有人(執行環境)，何來的 **走**、**跑**、**跳**？ 這專注於操縱外部實體的語言稱為腳本語言或是膠水語言。