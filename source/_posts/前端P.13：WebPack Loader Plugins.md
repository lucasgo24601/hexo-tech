---
title: 前端P.13：WebPack Loader Plugins
date: 2020-02-14 18:05:00
categories: 前端系列
comments: true
cover: https://i.imgur.com/NZmWawh.png
type: post
toc: true
toc_number: 250
copyright: false
tags: 
    - NodeJS
    - npm
    - packeage.json
    - Bable
    - Uglify
    - WebPack
---
# 🥜 前情提要
在上一回中，我們提到NodeJS可以安裝一些工具，幫助我們開發和出版，這邊介紹一下前端在CI/CD中很常使用的工具 WebPack。

# WebPack：
對於早期的前端來說，因為硬體和軟體的發展水平，當時開發很簡單，但是現代的前端使用了很多NodeJS工具實做前端，所以出版程式碼，就要使用一堆工具，情況如下
```
執行tsc
執行bable
執行gulp
執行....
```
明顯很愚蠢，所以有了WebPack。

- Webpack是一個模組打包器，他可以集成很多翻譯或是預處理的工具，像是 TypeScript、Babel、SASS ...etc。
- Webpack是一個具有多功能的優化工具，他可以幫助你 壓縮、複製、清除、抽取...etc。

總之WebPack的目的就是取代你那10幾個工具，因為WebPack自己就有集成那些知名的工具(Loader)，讓你使用這些工具更輕鬆容易。

## WebPack的Config功能：
根據官網介紹必須要有個檔案叫做webpack.config.js，其檔案裡面應該要寫這些配置。
![](https://i.imgur.com/JWCuKDd.png)

這些配置主要就是敘述：
- entry:定義輸入資源，把你要處理的js檔案放到這邊，可以單個也可以多個
- output:定義輸出資源，把最終處理的資源放到哪個資料夾以及名稱。

```
my-project/
├── file1.js
├── file2.js
└── webpack.config.js
```

```
const path = require("path");

module.exports = {
  entry: ['./file1.js', './file2.js'],
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist")
  }
};
```

以上設定就能把多個檔案合併成一個檔案，而這只是最基本的設定，下面介紹更多個功能。

## WebPack的Loader功能：
Webpack是模組打包器，就是因為它有個功能叫做`Loader`。

在之前的文章中介紹過TypeScript，所以我們也把他加入webpack家族，而這邊用兩個工具來模擬前端現在開發的數個工具。

### TypeScript Loader：
如果要讓webpack控管ts檔案，就必須使用 loader工具，它可以允許你在 webpack 構建流程中處理 TypeScript 檔案。

假設有個TS檔案名為`index.ts`

```
function hello(name: string) {
  console.log(`Hello, ${name}!`);
}

hello("World");
```
你可以在你的 `webpack.config.js` 檔案中使用 ts-loader 來處理這個檔案：
```
module.exports = {
  // 其他設定...
  module: {
    rules: [
      // 其他規則...
      {
        test: /\.ts$/,
        use: "ts-loader"
      }
    ]
  }
};
```
這樣，當 webpack 執行構建時，它會使用 ts-loader 轉譯 index.ts 檔案，並將它轉譯成 JavaScript 代碼。你就可以在你的應用程式中使用這個檔案了。

### Babel Loader：
上面的教學講了很多Babel的用途和好處，所以Babel也算是必要的工具
首先需要安裝Babel Loader和相應的Babel插件。
```
npm install -D babel-loader babel/core babel/preset-env
```

之後如果要將這個工具加入WebPack，必須加入這些WebPack.Config設定。
![](https://i.imgur.com/8eVev4o.png)
而如果你要設定Babel的選項的話，你可以採用這種加入方式。
![](https://i.imgur.com/TNOAG58.png)
那今天我們要Babael將 箭頭函數 轉換成標準化的函數，可以從 options 點進去看說明。
![](https://i.imgur.com/jBMBR9j.png)
所以我們Babel Loader 的WebPack.Config設定應該是長這樣：
![](https://i.imgur.com/Uuw0r5A.png)

## WebPack的Plugin功能：
Webpack是一個具有多功能的優化工具，就是因為它有個功能叫做`Plugin`。

假設有多個js檔案，例如
```
my-project/
├── app.js
├── another.js
└── webpack.config.js
```

而如果這兩個檔案有共用的程式碼的話，我們可以透過webpack進行優化：
```
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: './src/app.js',
    another: './src/another.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common' // 指定公共 bundle 的名稱。
    })
  ]
};
```
這樣 webpack 在建置時會將`./src/app.js`和`./src/another.js`兩個檔案打包成 `app.bundle.js`和`another.bundle.js`兩個檔案，並且在它們之間抽取出共用的程式碼並打包成一個名為`common.bundle.js`的公共檔案。

webpack官方支援很多plugins，每種都有特定的任務
- UglifyJSPlugin：壓縮並醜化 JavaScript 程式碼
- CommonsChunkPlugin：可以將公共的程式抽取到單獨的檔案，並讓被抽離的進行串接
- MiniCssExtractPlugin：將js中的css抽取出來成一個css文件。
- IgnorePlugin：用於過濾打包文件，減少打包體積大小
- ImageminPlugin：壓縮圖片
