---
title: 週報P.01 - MIME Types
date: 2022-11-08 13:37:00
categories: 週報系列
comments: true
cover: https://i.imgur.com/jdEP4N6.png
type: newsletter
toc: true
toc_number: 250
copyright: false
tags: 
    - 副檔名
    - MIME Types
    - HTTP
---
# 🚀 三句話總結
1. 知道副檔名機制的原理
2. 熟悉數據協定是在做什麼用
3. 了解HTTP協定中的Header欄位

# ☘️ 為什麼我要了解
因為副檔名機制在作業系統中至關重要，算是基本的軟體工程師必知的內容，尤其是使用一些盜版或是非法的資源，他們可能會有相關的保護機制，甚至在設計自己的產品時也會用到類似的保護機制，避免機密資料外洩。

# 🎨 改變了什麼
了解副檔名機制後，我有個軟體叫做「粒子工具」，該工具可以產出一個Config，可以直接套用在「粒子系統」，但是我希望保護Config不被人為修改又或是外流被別人知道內容。所以我運用副檔名系統的原理生成一個保護機制。

# ✍️ 總結和心得

## 作業系統如何區分資料要用什麼協議解析?
我們知道所有檔案都是由一堆 0 1 所組成，那麼當你要打開一個 1010101... 的資料，電腦要用哪種協議去解析這串資料呢？
![](https://i.imgur.com/ptWOG2I.jpg)
這世上有非常多種協議：mp3、doc、wav、txt、jpg ...etc 這些協議中的一部分就是定義資料要用什麼方式**按照設計好的規則排列**

Ex:
C:\Users\User\Desktop\testFile
```
mkdir test
```
在桌面上中有個檔案叫做testFile，他的內容由一些文字組成，而當你用 Windwos內建的記事本->開啟舊檔->testFile，那麼你將會看到
![](https://i.imgur.com/4pF0Txu.png)
但是當你用命令行開啟 testFile，就會執行 mkdir test，而剛好這指令在命令行中代表新增一個資料夾名為test，結果如下
![](https://i.imgur.com/HmzpRPs.png)
甚至更高端的玩法是，一個名為 MGK.jpg的圖片，當你用 Windows內建的相簿軟體 打開會有正常圖片
![](https://i.imgur.com/CzaWgys.png)
但是當你用 WinRAR 開啟這 MGK.jpg時，你就會發現隱藏的內容
![](https://i.imgur.com/bnq363i.png)
之所以能這樣做是因為這些資料經過**精心安排**，所以當使用jpg協議的規則排序去解析 1010101..etc 時能夠解析成正確的圖片，而當使用zip協議的規則排序去解析 1010101..etc 時則能解析成壓縮檔案方式。

> 以下指令就可以實現資料的**精心安排**，以此方式就能隱藏B.zip，只有當你用 壓縮程式 去打開 C.jpg 才能解析出 B.zip的實際內容
> ```
> copy /b A.jpg+B.zip C.jpg
> ```


而為了解決電腦不知道要用什麼軟體去解析這 1010101..etc 資料，所以在各種作業系統中都遵循著，副檔名機制，在作業系統中 testFile.jpg 這東西如果要打開，作業系統就知道這是「.jpg」，所以要用 jpg協議進行解析執行。

## 瀏覽器如何區分資料要用什麼協議解析? MIME
在作業系統我們可以使用副檔名標準進行解析，但是在瀏覽器中我們資料傳遞封包是用HTTP協議制定的，而在HTTP協議中規定了使用 MIME Types 標準來表示要用什麼協議解析，而非作業系統中常見的 副檔名標準 

在HTTP協議中定義一個欄位叫做 content-type 這個欄位就是用於紀錄檔案類型是什麼，也就是MIME Type，一些在作業系統中常見的副檔名在 MIME 中對應圖如下

| 副檔名| MIME Type|
| -------- | -------- |
| doc | application/msword |
| zip | application/zip |
| jpg | image/jpeg |
| txt | text/plain |

而當瀏覽器拿到 1010101..etc 的資料 他只用看 content-type  中記載的類型是什麼，就可以使用內建的解析器去解析出正確的內容。

這邊舉個例子:

https://www.google.com/images/branding/googlelogo/2x/googlelogo_light_color_272x92dp.png

以上為Google的logo圖片，而我們能在瀏覽器中正確看到圖片，只因為 content-type 記載他是png圖片，而絕對不是因為他的網址叫做 googlelogo_light_color_272x92dp.png，如果今天 content-type 記載的是 text/plain 那麼這資料將會用文字方式顯示。

所以在瀏覽器中**網址的副檔名**並不是決定瀏覽器要用什麼方式解析他，而是 content-type 中記載的 MIME Type決定。

## 結論
在作業系統中採用 副檔名 ，來實現雙擊左鍵後要用哪種協議解析
在瀏覽器中封包傳遞採用HTTP，在HTTP中採用 MIME Types，來實現收到資料後要用哪種協議解析

兩者的共同用途皆是，標記資料的格式是什麼，只是運用的場合不同，一個是 OS(Windwos、MAC ..etc)，另一個是 HTTP協議

那麼問題來了，透過瀏覽器接收封包，這些封包是HTTP協議，所以可以使用MIME Type解析，那...如果用瀏覽器開啟本地資料呢？他沒有封包傳遞，哪來的HTTP協定？

![](https://i.imgur.com/me1WeF0.png)
答案：沒有統一，只是Chrome採用辨別 副檔名機制 之後自動生成假的HTTP來實現 MIME Type解析功能，以此實現瀏覽器可以辨別本地檔案。